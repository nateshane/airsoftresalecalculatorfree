joCache.set("menu", function() {
    // some inline data and chaining going on here,
    // dont be afraid, it'll all make sense later
    var data = new joRecord({
        salePrice: "0",
        accPrice: "0",
        internalPrice: "0",
        skirmishCount: "0",
        generosity: "0",
        estimate: "0" 
    });
    this.data = data;

    var card = new joCard([
		new joFlexrow([ new joButton("Reset").selectEvent.subscribe(function() {$('input').val('0'); $('#currencyCodeInput').val(localStorage.defaultCurrency);}), 
			new joButton("About").selectEvent.subscribe(function() {
				App.scn.alert("Some systems, such as Polarstars and PTWs, are more likely to retain their value.<br/><br/>Keep this in mind when pricing your item, or evaluating one before a purchase.<br/><br/><br/>" +
							  "Formula adapted from Vorpalbunnie on reddit.com");
				})]),
        new joLabel("Retail Price of Gun"),
        new joFlexrow([new joInput(data.link("salePrice"))]
        ),
        new joLabel("Price of Accessories"),
        new joFlexrow([new joInput(data.link("accPrice"))]
        ),
        new joLabel("Price of Internal Upgrades"),
        new joFlexrow([new joInput(data.link("internalPrice"))]
        ),
        new joLabel("Months of Heavy Usage"),
        new joFlexrow([new joInput(data.link("skirmishCount")), new joButton("Help").selectEvent.subscribe(function() {
                App.scn.alert("Skirmishing more than twice per month is considered heavy usage");
            })]
        ),
        new joLabel("Generosity Multiplier"),
        new joFlexrow([new joInput(data.link("generosity")), new joButton("Help").selectEvent.subscribe(function() {
                App.scn.alert("A variable for how generous you are (subtracts multiplier * 10)");
            })]
        ),
        new joButton("Calculate").selectEvent.subscribe(function() {		
            var priceBeforeConversion = (parseFloat(data.getProperty("salePrice"))*0.70)+(data.getProperty("accPrice")*0.66)+(data.getProperty("internalPrice")*0.65)-(10*(data.getProperty("skirmishCount")/2)) - (10*data.getProperty("generosity"));
			
			if(priceBeforeConversion == NaN || priceBeforeConversion.toString() == "NaN"){
				App.scn.alert("Please check all fields for errors.");
			}
			else{	
				App.scn.alert("Your setup is worth $" + parseFloat(priceBeforeConversion).toFixed(2));
			}
        })
    ]);
	
    return card;
});